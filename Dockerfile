FROM anapsix/alpine-java
LABEL maintainer="devops@vibedesenv.com"
COPY target/discovery-msa-service-0.0.1-SNAPSHOT.jar /home/discovery-msa-service-0.0.1-SNAPSHOT.jar
CMD ["java","-Xmx128m","-jar", "-Dspring.profiles.active=cloud","/home/discovery-msa-service-0.0.1-SNAPSHOT.jar"]